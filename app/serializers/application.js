import DS from 'ember-data';

export default DS.RESTSerializer.extend({
    normalizeResponse(store, primaryModelClass, payload, id, requestType) {
        let tempPayload = {
            info: {
                id: payload.response.user.user_name,
                first_name: payload.response.user.first_name,
                last_name: payload.response.user.last_name,
                user_avatar: payload.response.user.user_avatar,
                user_avatar_hd: payload.response.user.user_avatar_hd,
                user_cover_photo: payload.response.user.user_cover_photo,
                total_badges: payload.response.user.stats.total_badges,
                total_friends: payload.response.user.stats.total_friends,
                total_checkins: payload.response.user.stats.total_checkins,
                total_beers: payload.response.user.stats.total_beers,
                total_created_beers: payload.response.user.stats.total_created_beers,
                total_followings: payload.response.user.stats.total_followings,
                total_photos: payload.response.user.stats.total_photos,
                date_joined: payload.response.user.date_joined,
            }
        };

        return this._super(store, primaryModelClass, tempPayload, id, requestType);
    },
});