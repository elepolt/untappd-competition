""" Create the release doc from the open issues for a given project and release title """
import json
import os
import requests


def main():
    """ The main function that does all the heavy lifting """

    base_url = 'https://api.untappd.com/v4/user/info/'
    client_stuff = '?client_id=' + \
        os.environ['UNTAPPD_CLIENT_ID'] + '&client_secret=' + \
        os.environ['UNTAPPD_CLIENT_SECRET']
    usernames = [
        'Soccer21x',
        'Andrewjb08',
        'matzahmike',
        'Esmith_22'
    ]

    # Make sure that the data folder is built
    try:
        os.makedirs('public/data/')
    except IOError:
        pass

    for username in usernames:
        url = base_url + username + client_stuff
        info = requests.get(url).json()

        file = open('public/data/' + username + '.json', 'w+')
        file.write(json.dumps(info))


if __name__ == "__main__":
    main()
