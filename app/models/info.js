import DS from 'ember-data';

export default DS.Model.extend({
    first_name: DS.attr('string'),
    last_name: DS.attr('string'),
    user_avatar: DS.attr('string'),
    user_avatar_hd: DS.attr('string'),
    user_cover_photo: DS.attr('string'),
    total_badges: DS.attr('number'),
    total_friends: DS.attr('number'),
    total_checkins: DS.attr('number'),
    total_beers: DS.attr('number'),
    total_created_beers: DS.attr('number'),
    total_followings: DS.attr('number'),
    total_photos: DS.attr('number'),
    date_joined: DS.attr('date'),
    joined: Ember.computed('date_joined', function(){
        let date_joined = this.get('date_joined');
        // console.log(moment());
        return date_joined;
    })
});