import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
    model: function(){ 
        let _this = this;
        return new Ember.RSVP.Promise(function(resolve){
            let promises = [];
            let users = [];
            let usernames = [
                'Soccer21x',
                'Andrewjb08',
                'matzahmike',
                'Esmith_22'
            ];

            usernames.forEach((username, index) => {
                promises[index] = _this.get('store').findRecord('info', username).then((results) => {
                    users.push(results);
                });
            });

            Ember.RSVP.all(promises).then(function(){
                resolve({users: users});
            })
        })
    },
    setupController(controller, model) {
        this._super(controller, model);
        model.users = model.users.sortBy('total_badges').reverse();
        model.currentWinner = model.users[0];
    }
});