import Ember from 'ember';

export default Ember.Controller.extend({
    users: [],
    currentWinner: null,
    order: 'asc',
    sort_column: '',
    actions: {
        sortColumn(columnName) {
            let sortedModel = this.model.users.sortBy(columnName) // automatically sorts it by asc
            if (columnName == this.get('sort_column') && this.get('order') == 'asc') {
                this.set('order', 'desc');
            } else {
                this.set('sort_column', columnName);
                this.set('order', 'asc');
                sortedModel = sortedModel.reverse();
            }

            this.set('model.users', sortedModel);
        }
    }
});