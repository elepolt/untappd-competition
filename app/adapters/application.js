import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    // Super custom url to just pull from the json files stored on the server
    buildURL: function() {
        // var normalURL = this._super.apply(this, arguments);
        return '/data/' + arguments[1] + '.json';
    },
});