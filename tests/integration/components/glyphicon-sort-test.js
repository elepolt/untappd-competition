import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('glyphicon-sort', 'Integration | Component | glyphicon sort', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{glyphicon-sort}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#glyphicon-sort}}
      template block text
    {{/glyphicon-sort}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
